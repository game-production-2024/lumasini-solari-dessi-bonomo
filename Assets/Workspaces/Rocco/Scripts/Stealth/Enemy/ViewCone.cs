using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ViewCone : MonoBehaviour
{
    public float VisionRange;
    public float VisionAngle;
    public LayerMask VisionObstructingLayer;//layer with objects that obstruct the enemy view, like walls, for example
    public int VisionConeResolution = 120;//the vision cone will be made up of triangles, the higher this value is the pretier the vision cone will be

    public Material basicMaterial;

    bool isColliding;
    [HideInInspector]public bool isCollidingForOthers;

    Mesh VisionConeMesh;
    MeshFilter MeshFilter_;
    [HideInInspector] public MeshRenderer MeshRenderer_;

    float Currentangle;
    Vector3 RaycastDirection;

    private void Awake()
    {
        MeshRenderer_ = transform.AddComponent<MeshRenderer>();

        MeshFilter_ = transform.AddComponent<MeshFilter>();
    }

    void Start()
    {
        

        MeshRenderer_.material = basicMaterial;

        VisionConeMesh = new Mesh();
        VisionAngle *= Mathf.Deg2Rad;
    }


    void Update()
    {
        DrawVisionCone();
    }

    void DrawVisionCone()//this method creates the vision cone mesh and check for collisions with the player
    {
        int[] triangles = new int[(VisionConeResolution - 1) * 3];
        Vector3[] Vertices = new Vector3[VisionConeResolution + 1];
        Vertices[0] = Vector3.zero;
        float Currentangle = -VisionAngle / 2;
        float angleIcrement = VisionAngle / (VisionConeResolution - 1);
        float Sine;
        float Cosine;

        for (int i = 0; i < VisionConeResolution; i++)
        {
            
            Sine = Mathf.Sin(Currentangle);
            Cosine = Mathf.Cos(Currentangle);
            RaycastDirection = (transform.up * Cosine * -1) + (transform.right * Sine * -1);
            Vector3 VertForward = (Vector3.up * Cosine * -1) + (Vector3.right * Sine * -1);

            if (Physics.Raycast(transform.position, RaycastDirection, out RaycastHit playerHit, VisionRange))
            {
                if (playerHit.transform.tag == "Player")
                {
                    if (!isColliding)
                    {
                        isColliding = true;
                    }
                }
            }

            if (Physics.Raycast(transform.position, RaycastDirection, out RaycastHit hit, VisionRange, VisionObstructingLayer))
            {
                Vertices[i + 1] = VertForward * hit.distance;
            }
            else
            {
                Vertices[i + 1] = VertForward * VisionRange;
            }

            Currentangle += angleIcrement;
        }

        isCollidingForOthers = isColliding;
        isColliding = false;

        for (int i = 0, j = 0; i < triangles.Length; i += 3, j++)
        {
            triangles[i] = 0;
            triangles[i + 1] = j + 1;
            triangles[i + 2] = j + 2;
        }
        VisionConeMesh.Clear();
        VisionConeMesh.vertices = Vertices;
        VisionConeMesh.triangles = triangles;
        MeshFilter_.mesh = VisionConeMesh;
    }
}