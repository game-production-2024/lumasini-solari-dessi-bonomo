using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    [Header("Patrolling parameters")]
    [Range(0f,90f)]public float angleOfRotation;
    private Vector3 eulerRotation;
    private Quaternion currentRotation;

    [SerializeField] private float timeOfRotation;
    [HideInInspector]public bool mustRotate = true;

    [Header("Spotting parameters")]
    public float timeToDie;

    [Tooltip("Velocot� di rotazione alla posizione del giocatore espressa in gradi al secondo")] //EX: 90, � un'anngolo retto al secondo, per girare di 180 gradi ci metter� 2 secondi
    [Range(0, 359)] public float turnSpeed;

    private PlayerDetector detectArea;

    void Start()
    {
        detectArea = GetComponentInChildren<PlayerDetector>();

        eulerRotation = new Vector3(0, 0, angleOfRotation);
        currentRotation.eulerAngles = eulerRotation;

        transform.rotation = currentRotation;
    }

    void Update()
    {
        if (mustRotate)
        {
            StartCoroutine(FlipAction());
        }
    }


    private void Flip()
    {
        if (mustRotate)
        {
            currentRotation.z *= -1f;
            transform.rotation = currentRotation;
        }
    }
    
    public IEnumerator FlipAction()
    {
        Flip();
        mustRotate = false;
        yield return new WaitForSeconds(timeOfRotation);
        mustRotate = true;
    }

    public IEnumerator SpotPlayer(GameObject player)
    {
        StartCoroutine(LookAtPlayer(player));

        detectArea.areaSprite.color = Color.red;

        if (!detectArea.isPlayerSpotted)
        {
            detectArea.areaSprite.color = Color.yellow;
            StopCoroutine(SpotPlayer(player));
            mustRotate = true;
        }

        yield return new WaitForSeconds(timeToDie);

        if (detectArea.isPlayerSpotted)
        {
            player.SetActive(false);
        }

        detectArea.areaSprite.color = Color.yellow;
        mustRotate = true;
        detectArea.isPlayerSpotted = false;
        StopAllCoroutines();
    }

    private IEnumerator LookAtPlayer(GameObject player)
    {
        while (detectArea.isPlayerSpotted)
        {
            Vector3 dirToLook = (player.transform.position - transform.position).normalized;
            float targetAngle = 180 - Mathf.Atan2(dirToLook.x, dirToLook.y) * Mathf.Rad2Deg;

            float angle = Mathf.MoveTowardsAngle(transform.eulerAngles.z, targetAngle, turnSpeed * Time.deltaTime);
            transform.eulerAngles = new Vector3(0, 0, angle);
            yield return null;
        }
    }

}
