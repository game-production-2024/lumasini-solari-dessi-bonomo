using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetector : MonoBehaviour
{
    public GameObject parent;
    public GameObject player;
    public GameObject PlayerTrigger;
    private CameraMovement camera_movement;
    [HideInInspector]public SpriteRenderer areaSprite;

    [HideInInspector]public bool isPlayerSpotted = false;
    

    private void Start()
    {
        camera_movement = parent.GetComponent<CameraMovement>();
        areaSprite = GetComponent<SpriteRenderer>();
    }

    private void Spot()
    {
        if (!isPlayerSpotted)
        {
            isPlayerSpotted = true;
            camera_movement.mustRotate = false;

            camera_movement.StopAllCoroutines();
            StartCoroutine(camera_movement.SpotPlayer(player));
        }
        
    }
 
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && !PlayerTrigger.GetComponent<PlayerStealthStatus>().isHidden)
        {
            Spot();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
            {
            if (PlayerTrigger.GetComponent<PlayerStealthStatus>().isHidden)
            {
                isPlayerSpotted = false;
            }
            else
            {
                Spot();
            }
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            isPlayerSpotted = false;
        }
    }

}
