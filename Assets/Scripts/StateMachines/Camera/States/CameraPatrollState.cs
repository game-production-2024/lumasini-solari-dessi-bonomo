using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPatrollState : CameraBaseState
{

    Vector3 eulerRotation;
    Quaternion currentRotation;
    public bool mustRotate = true;

    public override void EnterState(CameraStateManager camera)
    {

        camera.viewCone.MeshRenderer_.material = camera.patrollMaterial;
        AudioManager.instance.Stop("Telecamera Allertata");

        camera.patrol_animation.enabled = true;
        camera.patrol_animation.Play();

        camera.sliderToDie.enabled = false;
    }

    public override void UpdateState(CameraStateManager camera)
    {
        if (camera.viewCone.isCollidingForOthers && !camera.player.GetComponent<PlayerStealthStatus>().isHidden)
        {
            camera.SwitchStateWithTimer(camera.SpottedState, camera.timeToSpot);
        }
        if (!camera.viewCone.isCollidingForOthers)
        {
            camera.count = 0f;
        }
    }
    void FlipAction(CameraStateManager camera)
    {
        currentRotation.z *= -1f;
        camera.transform.rotation = currentRotation;
    }
}