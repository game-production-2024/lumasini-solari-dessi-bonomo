using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSpottedState : CameraBaseState 
{    
    float count = 0f;

    public override void EnterState(CameraStateManager camera)
    {
        AudioManager.instance.Play("Telecamera Allertata");
        camera.viewCone.MeshRenderer_.material = camera.spottedMaterial;
        camera.patrol_animation.enabled = false;
        camera.coneOfView.SetActive(true);
        count = 0;
    }

    public override void UpdateState(CameraStateManager camera)
    {
        if (count <= camera.timeToDie)
        {
            Vector3 dirToLook = (camera.player.transform.position - camera.transform.position).normalized;
            float targetAngle = 180 - Mathf.Atan2(dirToLook.x, dirToLook.y) * Mathf.Rad2Deg;

            float angle = Mathf.MoveTowardsAngle(camera.transform.eulerAngles.z, targetAngle, camera.turnSpeed * Time.deltaTime);
            camera.transform.eulerAngles = new Vector3(0, 0, angle);

            count += Time.deltaTime;


            camera.sliderToDie.enabled = true;
            float t = Mathf.InverseLerp(camera.timeToDie, 0.0f, count);
            camera.sliderToDie.fillAmount = t;

            if (!camera.viewCone.isCollidingForOthers || camera.player.GetComponent<PlayerStealthStatus>().isHidden)
            {
                camera.SwitchState(camera.PatrolState);
            }

            if (count <= 0.0f)
            {
                camera.sliderToDie.fillAmount = 0.0f;
            }
        }
        else
        {
            AudioManager.instance.Play("Game Over");
            Vector3 position = camera.player.GetComponent<Respawn>().spawnPoint.transform.position;
            camera.Respawn(camera.player, position);
            count = 0f;
            camera.SwitchState(camera.PatrolState);
        }
    }
}
