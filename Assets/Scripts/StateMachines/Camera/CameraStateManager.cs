using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraStateManager : StateManagerBase
{
    CameraBaseState currentState;
    public CameraPatrollState PatrolState = new CameraPatrollState();
    public CameraSpottedState SpottedState = new CameraSpottedState();

    /*
    [Header("Patrolling parameters")] //Sarebbe interessante inserire i parametri dello stato interni alla classe e renderli visibili nell'inspector dello state manager
    [Range(0f, 90f)] public float angleOfRotation;
    public float timeOfRotation;*/
    [HideInInspector] public Animation patrol_animation;

    [Header("Waiting Parameter")]
    public float timeToSpot;


    [Header("Spotting parameters")]

    [Tooltip("Caricare l'oggetto che fa da cono di visione, con attaccato uno spriteRenderer e un collider2D")]
    public GameObject coneOfView;
    public GameObject player;
    public float timeToDie;
    [Tooltip("Velocot� di rotazione alla posizione del giocatore espressa in gradi al secondo")] //EX: 90, � un'anngolo retto al secondo, per girare di 180 gradi ci metter� 2 secondi
    [Range(0, 359)] public float turnSpeed;

    public Material patrollMaterial;
    public Material spottedMaterial;

    [HideInInspector] public ViewCone viewCone;
    [HideInInspector] public Collider2D coneCollider;

    [Header("Debugging parameters")]
    public Image sliderToDie;

    [HideInInspector]public float count;
    /*
    [Header("Design parameters")]
    [Tooltip("Usata per far iniziare le telecamere con una rotazione invertita")]
    public bool invertRotation;*/

    void Start()
    {
        viewCone = coneOfView.GetComponent<ViewCone>();
        coneCollider = coneOfView.GetComponent<Collider2D>();
        patrol_animation = GetComponent<Animation>();

        currentState = PatrolState;

        currentState.EnterState(this);
    }

    void Update()
    {
        currentState.UpdateState(this);
    }

    public void SwitchState(CameraBaseState state)
    {
        currentState = state;

        currentState.EnterState(this);
    }

    public void SwitchStateWithTimer(CameraBaseState state, float timer)
    {
        if (count <= timer)
        {
            count += Time.deltaTime;
        }
        else
        {
            currentState = state;
            currentState.EnterState(this);

            count = 0;
        }
            
    }

    public void Respawn(GameObject oggetto, Vector3 position)
    {
        oggetto.transform.position = position;
    }
}
