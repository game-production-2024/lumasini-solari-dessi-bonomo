using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConeOfView : MonoBehaviour
{
    [Tooltip("Caricare il parent del Cono di Visione.\n" +
        "Assicurarsi che abbia come component una StateMachine")]
    public GameObject parent;
    CameraStateManager stateManager;

    private void Start()
    {
        stateManager = parent.GetComponent<CameraStateManager>();
    }

}
