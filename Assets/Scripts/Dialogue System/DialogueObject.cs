using UnityEngine;

[CreateAssetMenu(menuName = "Dialogue/DialogueObject")]
public class DialogueObject : ScriptableObject
{
    [SerializeField] [TextArea] private string[] dialogue;
    [SerializeField] private Response[] responses;
    [SerializeField] private Sprite[] sprites;
    [SerializeField] private SpritePosition[] spritePositions;

    public string[] Dialogue => dialogue;
    public bool HasResponses => Responses != null && Responses.Length > 0;
    public Response[] Responses => responses;
    public Sprite[] Sprites => sprites;
    public SpritePosition[] SpritePositions => spritePositions;

    private void OnValidate()
    {
        if (sprites != null && dialogue != null && sprites.Length != dialogue.Length)
        {
            Debug.LogWarning("the number of sprites does not match the number of dialogue lines");
        }
    }

    public DialogueObject()
    {
        Debug.Log("New DialogueObject instance created!");
    }
}

public enum SpritePosition
{
    Left,
    Right
}






