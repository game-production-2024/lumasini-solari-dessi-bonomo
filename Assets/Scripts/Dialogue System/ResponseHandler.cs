using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections.Generic;

public class ResponseHandler : MonoBehaviour
{
    [SerializeField] private RectTransform responseBox;
    [SerializeField] private RectTransform responseButtonTemplate;
    [SerializeField] private RectTransform responseContainer;

    private DialogueUI dialogueUI;
    private ResponseEvent[] responseEvents;
    private Button responseButton; // Riferimento al pulsante di risposta

    private List<GameObject> tempResponseButtons = new List<GameObject>(); // per il bottone della scelta del dialogo

    private void Start()
    {
        dialogueUI = GetComponent<DialogueUI>();
    }

    private void Update()
    {
        if (responseButton != null && Input.GetKeyDown(KeyCode.Space))
        {
            responseButton.onClick.Invoke();
        }
    }

    public void AddResponseEvents(ResponseEvent[] responseEvents)
    {
        this.responseEvents = responseEvents;
    }

    public void ShowResponses(Response[] responses)
    {
        float responseBoxHeight = 0;

        for (int i = 0; i < responses.Length; i++)
        {
            Response response = responses[i];
            int responseIndex = i;

            GameObject responseButtonObject = Instantiate(responseButtonTemplate.gameObject, responseContainer);
            responseButtonObject.gameObject.SetActive(true);
            responseButtonObject.GetComponent<TMP_Text>().text = response.ResponseText;
            Button button = responseButtonObject.GetComponent<Button>();
            button.onClick.AddListener(() => OnPickedResponse(response, responseIndex));

            tempResponseButtons.Add(responseButtonObject); // per il bottone della scelta del dialogo

            // Imposta il primo (o unico) pulsante come pulsante di risposta
            if (i == 0)
            {
                responseButton = button;
            }

            responseBoxHeight += responseButtonTemplate.sizeDelta.y;
        }

        responseBox.sizeDelta = new Vector2(responseBox.sizeDelta.x, responseBoxHeight);
        responseBox.gameObject.SetActive(true);
    }

    private void OnPickedResponse(Response response, int responseIndex)
    {
        responseBox.gameObject.SetActive(false);

        foreach (GameObject button in tempResponseButtons)
        {
            Destroy(button);
        }
        tempResponseButtons.Clear();
        responseButton = null; // Resetta il pulsante di risposta

        if (responseEvents != null && responseIndex < responseEvents.Length)
        {
            responseEvents[responseIndex].OnPickedResponse?.Invoke();
        }

        responseEvents = null;

        if (response.DialogueObject)
        {
            dialogueUI.ShowDialogue(response.DialogueObject);
        }
        else
        {
            dialogueUI.CloseDialogueBox();
        }
    }
}
