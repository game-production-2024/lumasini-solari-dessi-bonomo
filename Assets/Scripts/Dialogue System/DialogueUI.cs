using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueUI : MonoBehaviour
{
    [SerializeField] private GameObject dialogueBox;
    [SerializeField] private TMP_Text textLabel;
    [SerializeField] private Image leftDialogueImage;
    [SerializeField] private Image rightDialogueImage;
    [SerializeField] private Animator dialogueBoxAnimator;
    //[SerializeField] private DialogueObject testDialogue;

    public bool IsOpen { get; private set; }

    private ResponseHandler responseHandler;
    private TypeWriterEffect typewriterEffect;

    private void Start()
    {
        typewriterEffect = GetComponent<TypeWriterEffect>();
        responseHandler = GetComponent<ResponseHandler>();

        CloseDialogueBox();
        //ShowDialogue(testDialogue);
    }

    public void ShowDialogue(DialogueObject dialogueObject )
    {
        if (dialogueObject == null)
        {
            Debug.LogError("DialogueObject is null!");
            return;
        }

        if (this == null)
        {
            Debug.LogError("DialogueUI instance is null!");
            return;
        }

        IsOpen = true;
        dialogueBox.SetActive(true);

        dialogueBoxAnimator.Play("animazione inizio dialogo");

        StartCoroutine(StepTroughDialogue(dialogueObject));
    }

    public void AddResponseEvents(ResponseEvent[] responseEvents)
    {
        responseHandler.AddResponseEvents(responseEvents);
    }

    private IEnumerator StepTroughDialogue(DialogueObject dialogueObject)
    {
      
        for (int i = 0; i < dialogueObject.Dialogue.Length; i++) 
        {
            string dialogue = dialogueObject.Dialogue[i];
            Sprite sprite = dialogueObject.Sprites.Length > i ? dialogueObject.Sprites[i] : null;
            SpritePosition position = dialogueObject.SpritePositions.Length > i ? dialogueObject.SpritePositions[i] : SpritePosition.Left;

            if (position == SpritePosition.Left)
            {
                leftDialogueImage.sprite = sprite;
                leftDialogueImage.enabled = sprite != null;
                rightDialogueImage.sprite = null;
                rightDialogueImage.enabled = false;
            }
            else
            {
                rightDialogueImage.sprite = sprite;
                rightDialogueImage.enabled = sprite != null;
                leftDialogueImage.sprite = null;
                leftDialogueImage.enabled = false;
            }

            yield return RunTypingEffect(dialogue);

            textLabel.text = dialogue;
            

            if (i == dialogueObject.Dialogue.Length - 1 && dialogueObject.HasResponses) break; //has responses � parte del codice di dialogueobject attenzione..

            yield return null;
            yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Space));
        }

        if (dialogueObject.HasResponses) 
        {
            responseHandler.ShowResponses(dialogueObject.Responses);
        }
        else 
        {
            CloseDialogueBox();
        }
    }

    private IEnumerator RunTypingEffect(string dialogue)//per skippare i dialoghi
    {
        typewriterEffect.Run(dialogue, textLabel);

        while (typewriterEffect.IsRunning)
        {
            yield return null;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                typewriterEffect.Stop();
            }
        }
    }
    
    public void CloseDialogueBox()
    {
        IsOpen = false;
        //dialogueBox.SetActive(false);
        //textLabel.text = string.Empty;
        //leftDialogueImage.sprite = null;
        //leftDialogueImage.enabled = false; 
        //rightDialogueImage.sprite = null;
        //rightDialogueImage.enabled = false;

        dialogueBoxAnimator.Play("animazione fine dialogo");
    }


}
