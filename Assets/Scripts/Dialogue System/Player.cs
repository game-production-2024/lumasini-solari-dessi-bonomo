
using UnityEngine;


public class Player : MonoBehaviour
{
    //per il sistema di dialogo
    [SerializeField] private DialogueUI dialogueUI;

    public DialogueUI DialogueUI => dialogueUI;

    public IInteractable Interactable { get; set; }
    //fine sistema di dialogo

    

    

    void Update()
    {
        if (dialogueUI.IsOpen) return; // per non far muovere il personaggio durante i dialoghi
        // Check for input and move the player
   
        // per il sistema di dialogo
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (Interactable != null)
            {
                Interactable.Interact(this);
            }
        }

    }

}
