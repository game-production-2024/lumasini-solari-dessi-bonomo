using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TypeWriterEffect : MonoBehaviour
{
    [SerializeField] public float typewriterSpeed = 50f;

    //per skippare i dialoghi
    public bool IsRunning { get; private set; }

    //per far fare una pausa quando ce un dialogo che termina con un punto o altri segni di punteggiatura..
    private readonly List<Punctuation> punctuations = new List<Punctuation>()
    {
        new Punctuation(new HashSet<char>(){'.', '!', '?'}, 0.7f),
        new Punctuation(new HashSet<char>(){',', ';', ':'}, 0.3f),
    };

    private Coroutine typingCoroutine; //per skippare i dialoghi 

    // importante per leggere la routine 
    public void Run(string textToType, TMP_Text textLabel)
    {
        if (IsRunning)  // per il bug del ontriggerenter
        {
            Stop();
        }
        typingCoroutine = StartCoroutine(routine: TypeText(textToType, textLabel));
    }
    
    public void Stop()
    {
        if (typingCoroutine != null)//per il bug dell'ontriggerenter
        {
            StopCoroutine(typingCoroutine);
            typingCoroutine = null;
        }
        IsRunning = false;
    }

    // importante per scrivere il testo
    private IEnumerator TypeText(string textToType, TMP_Text textLabel)
    {
        IsRunning = true; //per skippare i dialoghi
        textLabel.text = string.Empty;

        float t = 0;
        int charIndex = 0;

        while(charIndex < textToType.Length)
        {
            int lastCharIndex = charIndex;//per far fare una pausa quando ce un dialogo che termina con un punto ecc..

            t += Time.deltaTime * typewriterSpeed;

            charIndex = Mathf.FloorToInt(t);
            charIndex = Mathf.Clamp(charIndex, 0, textToType.Length);

            for (int i = lastCharIndex; i < charIndex; i++) //per far fare una pausa quando ce un dialogo che termina con un punto ecc..
            {
                bool isLast = i >= textToType.Length - 1;

                textLabel.text = textToType.Substring(0, i + 1);

                if (IsPunctuation(textToType[i], out float waitTime) && !isLast && !IsPunctuation(textToType[i + 1], out _))//per far fare una pausa quando ce un dialogo che termina con un punto ecc..
                {
                    yield return new WaitForSeconds(waitTime);
                }
            }

            //textLabel.text = textToType.Substring(0, charIndex);

            yield return null;
        }

        IsRunning = false; //per skippare i dialoghi
        //textLabel.text = textToType;
    }

    //per far fare una pausa quando ce un dialogo che termina con un punto ecc..
    private bool IsPunctuation(char character, out float waitTime)
    {
        foreach (Punctuation punctuationCategory in punctuations)
        {
            if (punctuationCategory.Punctuations.Contains(character))
            {
                waitTime = punctuationCategory.WaitTime;
                return true;
            }
        }

        waitTime = default;
        return false;
    }

    private readonly struct Punctuation
    {
        public readonly HashSet<char> Punctuations;
        public readonly float WaitTime; 

        public Punctuation(HashSet<char> punctuations, float waitTime)
        {
            Punctuations = punctuations;
            WaitTime = waitTime;
        }
    }
}
