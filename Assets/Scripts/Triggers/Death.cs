using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            Vector3 position = collision.GetComponent<Respawn>().spawnPoint.transform.position;
            collision.transform.position = position;
        }
    }
}
