using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewOnTriggerEnter : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        // Check if the player has entered the trigger
        if (other.gameObject.CompareTag("Player"))
        {
            // Get the DialogueActivator instance
            DialogueActivator dialogueActivator = GetComponent<DialogueActivator>();

            // Automatically show the dialogue
            dialogueActivator.Interact(other.GetComponent<Player>());
        }
    }
}
