using UnityEngine;
using UnityEngine.Events;
public class TriggerDetector : MonoBehaviour
{
    public string otherTag;
    public UnityEvent onTriggerEnter;
    public UnityEvent onTriggerExit;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(otherTag))
        {
            onTriggerEnter.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag(otherTag))
        {
            onTriggerExit.Invoke();
        }
    }
}
