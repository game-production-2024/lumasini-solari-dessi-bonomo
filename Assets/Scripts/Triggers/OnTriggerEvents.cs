using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerEvents : MonoBehaviour
{
    public Player player;
    public DialogueActivator dialogueActivator;
    public Collider2D collidertoSwitchOff;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        dialogueActivator.Interact(player);
        collidertoSwitchOff.enabled = false;
       
    }
}
