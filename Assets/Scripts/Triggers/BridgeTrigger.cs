using UnityEngine;
using UnityEngine.Events;

public class BridgeTrigger : MonoBehaviour
{
    public string otherTag;
    public UnityEvent onTriggerEnter;
    public UnityEvent onTriggerExit;

    public bool isBroken;

    GameObject ponte;
    Animator anim;

    void Start()
    {
        ponte = GameObject.FindGameObjectWithTag("Bridge");
        anim = ponte.GetComponent<Animator>();
    }

    void Update()
    {
        anim.SetBool("isBroken", isBroken);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(otherTag))
        {
            onTriggerEnter.Invoke();
            isBroken = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag(otherTag))
        {
            onTriggerExit.Invoke();
        }
    }
}
