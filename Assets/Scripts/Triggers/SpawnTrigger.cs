using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTrigger : MonoBehaviour
{
    public Respawn player;
    public GameObject fire;
    public GameObject trigger;
    public bool acceso = false;

    SpriteRenderer spriteRenderer;
    Animator anim;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        anim = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        anim.SetBool("Acceso", acceso);

        if (player.spawnPoint != transform)
        {
            acceso = false;
            trigger.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        player.spawnPoint = transform;
        if (!acceso)
        {
            AudioManager.instance.Play("Fuoco Barile Acceso");
            AudioManager.instance.Play("Fuoco");
        }
        acceso = true;
        trigger.SetActive(true);
    }
}
