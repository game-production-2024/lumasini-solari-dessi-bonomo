using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximityPopup : MonoBehaviour
{

    [Tooltip("Inserire l'oggetto popup che si vuole mostrare quando ci si avvicina all'oggetto")]
    public GameObject popup;

    private void Update()
    {
        if (popup.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                popup.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (popup != null)
            {
                popup.SetActive(true);
            }
        }
        
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (popup != null)
            {
                popup.SetActive(false);
            }
            
        }
        
    }
}
