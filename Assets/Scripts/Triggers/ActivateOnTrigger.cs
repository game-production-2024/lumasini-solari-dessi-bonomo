using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOnTrigger : MonoBehaviour
{
    public GameObject objectToActivate; // Assign the GameObject you want to activate/deactivate in the Inspector
    public bool activate = true; // Choose whether to activate (true) or deactivate (false) the object

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Check if the player collided with the trigger
        if (collision.gameObject.CompareTag("Player"))
        {
            // Activate or deactivate the GameObject based on the activate variable
            objectToActivate.SetActive(activate);
        }
    }
}
