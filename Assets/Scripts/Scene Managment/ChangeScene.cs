using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    

    public void SwitchScene(string sceneName) 
    {
        Debug.Log("SceneManager: Loading scene " + sceneName);
        SceneManager.LoadScene(sceneName);
    }


}
