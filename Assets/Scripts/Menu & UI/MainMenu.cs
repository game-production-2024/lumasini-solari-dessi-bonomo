using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public string scenaPrincipale = "Scena di Prova";


    private void Start()
    {
        AudioManager.instance.Play("Soundtrack");
        AudioManager.instance.StopIfIsPlaing("Musica finale", "Soundtrack");
        AudioManager.instance.StopIfIsPlaing("Musica Scelta finale", "Soundtrack");
    }

    // Metodo per iniziare il gioco
    public void PlayGame()
    {
        // Carica la scena del gioco, sostituisci "GameScene" con il nome della tua scena di gioco
        SceneManager.LoadScene(scenaPrincipale);
    }

    // Metodo per aprire le opzioni
    public void OpenOptions()
    {
        // Codice per aprire il menu delle opzioni
        Debug.Log("Opzioni aperte!");
    }

    // Metodo per uscire dal gioco
    public void QuitGame()
    {
        // Uscire dal gioco
        Debug.Log("Gioco chiuso!");
        Application.Quit();
    }
}
