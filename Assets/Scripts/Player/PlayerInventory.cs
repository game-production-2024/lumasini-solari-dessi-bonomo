using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : MonoBehaviour
{

    public bool hasKey = false;
    public List<Key> keyList;
    public Image uiImage;
    public GameObject UiCanvas;

    private void Start()
    {
        AudioManager.instance.Stop("Soundtrack");
        UiCanvas.SetActive(false);
    }

    public void Update()
    {
        if (keyList != null)
        {
            foreach (Key key in keyList)
            {
                if (key.isActive == true)
                {
                    uiImage.sprite = key.keySprite;
                    hasKey = true;

                    if (!UiCanvas.activeSelf)
                    {
                        UiCanvas.SetActive(true);
                    }
                }
            }
        }
    }
    public void GiveKey()
    {
        hasKey = true;
    }
    public void GiveKeyObject(Key key)
    {
        UiCanvas.SetActive(true);
        keyList.Add(key);
        hasKey = true;
    }

    public void TakeKeyAway()
    {
        hasKey = false;
        UiCanvas.SetActive(false);
        if (keyList != null)
        {
            foreach (Key key in keyList)
            {
                if (key.isActive == true)
                {
                    key.isActive = false;
                    uiImage.sprite = null;
                }
            }
        }  
    }
}
