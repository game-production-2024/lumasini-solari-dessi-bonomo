using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    /*Questo script serve per:
     *  1. Muovere il personaggio orizzontalmente
     *  2. Abbassarsi
     *  3. Scivolata
     *  4. Arrampicata verticale
     */

    // Inizializzazione delle variabili
    private Rigidbody2D rb;
    private Animator animator;

    [Header("Player Movement")]
    [Tooltip("La velocit� di movimento del giocatore")]
    [SerializeField] private float speed;

    [Tooltip("La velocit� di movimento del giocatore abbassato")]
    [SerializeField] private float crouchSpeed;

    // Variabili di Input del giocatore
    private float horizontal;
    private float vertical;

    // Variabili logiche per flip e la velocit�
    private bool isFacingLeft = true;
    private bool isFacingLeftWhileClimb;
    private float currentSpeed;

    [Header("Crouch Size")]
    [Tooltip("Collider principale del personaggio")]
    [SerializeField] private GameObject mainCollider;

    [Tooltip("Collider del personaggio per abbassarsi")]
    [SerializeField] private GameObject crouchCollider;

    [Header("Dash")]
    [Tooltip("Velocit� di movimento della scivolata del personaggio")]
    [SerializeField] private float dashingPower = 24f;

    [Tooltip("Tempo di scivolata del personaggio")]
    [SerializeField] private float dashingTime = 0.2f;

    [Tooltip("Tempo tra una scivolata e l'altra")]
    [SerializeField] private float dashingCooldown = 1;

    private bool canDash = true;
    private bool isDashing;

    public bool isCrouched;

    bool canStand = true;

    private Player dialogue;

    [Header("Graphics")]
    [Tooltip("GameObject che contiene il componente Animator")]
    [SerializeField] private GameObject gfx;

    [Header("Climbing")]
    [Tooltip("La velocit� di arrampicata del giocatore")]
    [SerializeField] private float climbingSpeed = 5f;

    Vector3 climbDirection;

    private bool isClimbing;
    private bool canClimb;

    private string walkName;

    // Start is called before the first frame update
    void Start()
    {
        // Assegnazione del Rigidbody
        rb = GetComponent<Rigidbody2D>();
        dialogue = GetComponent<Player>();
        animator = gfx.GetComponent<Animator>();

        currentSpeed = speed;
        walkName = "Camminata Player";
    }

    // Update is called once per frame
    void Update()
    {
        if (dialogue.DialogueUI.IsOpen)
        {
            horizontal = 0f;
            UpdateAnimator();
            return;
        }

        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");

        if (isDashing)
        {
            UpdateAnimator();
            return;
        }

        AudioManager.instance.PlayWhileHolding(walkName, horizontal);

        if (vertical < 0f && !isClimbing)
        {
            Crouch();
        }
        if (canStand && vertical >= 0 && !isClimbing)
        {
            Stand();
        }

        if (Input.GetKeyDown(KeyCode.LeftShift) && canDash && !isCrouched)
        {
            StartCoroutine(Dash());
        }

        if (canClimb && Mathf.Abs(vertical) > 0f)
        {
            isClimbing = true;
        }
        else if (!canClimb)
        {
            isClimbing = false;
        }

        Flip();
        UpdateAnimator();
    }

    private void FixedUpdate()
    {
        if (dialogue.DialogueUI.IsOpen)
        {
            rb.velocity = Vector3.zero;
            return;
        }

        if (isDashing)
        {
            return;
        }

        if (isClimbing)
        {
            rb.velocity = new Vector2(0f, vertical * climbingSpeed);
        }
        else
        {
            rb.velocity = new Vector2(horizontal * currentSpeed, rb.velocity.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Upper Wall"))
        {
            canStand = false;
        }

        if(collision.CompareTag("Stair Ground"))
        {
            canClimb = false;
        }
        else if (collision.CompareTag("Climbable"))
        {
            canClimb = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Upper Wall"))
        {
            canStand = true;
        }

        if (collision.CompareTag("Climbable"))
        {
            canClimb = false;
            if(isFacingLeftWhileClimb != isFacingLeft)
            {
                transform.localScale = climbDirection;
                isFacingLeft = isFacingLeftWhileClimb;
            } 
        }
    }

    // Flip del giocatore in base alla direzione che sta guardando
    private void Flip()
    {

        if (!isClimbing)
        {
            if (isFacingLeft && horizontal > 0f || !isFacingLeft && horizontal < 0f)
            {
                isFacingLeft = !isFacingLeft;
                Vector3 localscale = transform.localScale;
                localscale.x *= -1f;
                climbDirection = transform.localScale;
                transform.localScale = localscale;
            }
        }
        else
        {
            isFacingLeftWhileClimb = isFacingLeft;
            if (isFacingLeftWhileClimb && horizontal > 0f || !isFacingLeftWhileClimb && horizontal < 0f)
            {
                isFacingLeftWhileClimb = !isFacingLeftWhileClimb;
                Vector3 localscale = transform.localScale;
                localscale.x *= -1f;
                climbDirection = localscale;
            }
        }
    }

    private void Crouch()
    {
        if (!isCrouched)
        {
            crouchCollider.GetComponent<Collider2D>().isTrigger = false;
            mainCollider.GetComponent<Collider2D>().isTrigger = true;
            currentSpeed = crouchSpeed;
            walkName = "Camminata Player Crouch";

        }
        isCrouched = true;
    }

    private void Stand()
    {
        if (isCrouched)
        {
            mainCollider.GetComponent<Collider2D>().isTrigger = false;
            crouchCollider.GetComponent<Collider2D>().isTrigger = true;
            currentSpeed = speed;
            walkName = "Camminata Player";
        }
        isCrouched = false;
    }

    private IEnumerator Dash()
    {
        canDash = false;
        isDashing = true;
        float originalGravity = rb.gravityScale;
        rb.gravityScale = 0f;
        if (isClimbing && isFacingLeft != isFacingLeftWhileClimb)
        {
            rb.velocity = new Vector2(climbDirection.x * dashingPower * -1, 0f);
            transform.localScale = climbDirection;
            isFacingLeft = isFacingLeftWhileClimb;
            isClimbing = false;
        }
        else
        {
            rb.velocity = new Vector2(transform.localScale.x * dashingPower * -1, 0f);
        }
        
        UpdateAnimator();
        Crouch();
        

        AudioManager.instance.Play("Dash");

        yield return new WaitForSeconds(dashingTime);

        rb.gravityScale = originalGravity;
        isDashing = false;
        if (canStand)
        {
            Stand();
        }

        yield return new WaitForSeconds(dashingCooldown);

        canDash = true;
    }

    // Aggiorna l'animatore con gli stati attuali
    private void UpdateAnimator()
    {
        animator.SetFloat("Speed", Mathf.Abs(horizontal));
        animator.SetBool("IsCrouched", isCrouched);
        animator.SetBool("IsDashing", isDashing);
        animator.SetBool("IsClimbing", isClimbing);
    }
}