using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStealthStatus : MonoBehaviour
{
    public bool isHidden = false;
    PlayerMovement player;

    void Start()
    {
        player = transform.GetComponent<PlayerMovement>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Shadow" && player.isCrouched)
        {
            isHidden = true;
        }
        else
        {
            isHidden = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Shadow")
        {
            isHidden = false;
        }
    }
}
