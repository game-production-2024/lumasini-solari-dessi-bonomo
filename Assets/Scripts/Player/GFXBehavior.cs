using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GFXBehavior : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    PlayerStealthStatus playerStatus;
    public GameObject player;

    void Start()
    {
        playerStatus = player.GetComponent<PlayerStealthStatus>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
       if (playerStatus.isHidden)
       {
        spriteRenderer.color = Color.grey;
       }
       else
       {
        spriteRenderer.color = Color.white;
       }
    }

}
