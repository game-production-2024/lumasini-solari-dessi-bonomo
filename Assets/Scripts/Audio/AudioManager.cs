using UnityEngine.Audio;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{

    public Sound[] sounds;

    public static AudioManager instance;

    // Start is called before Start
    void Awake()
    {
        //Controllo per il passaggio da una scena all'altra dell'Audio Manager
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        //Caricamento e inizializzazione dei suoni in storage
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    //Metodo per far partire una traccia audio
    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound " + name + " not Found!");
            return;
        }
        s.source.Play();
    }

    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound " + name + " not Found!");
            return;
        }
        s.source.Stop();
    }

    public void PlayWhileHolding(string name, float value)
    {
        if (value == 1 || value == -1)
        {
            Sound s = Array.Find(sounds, sound => sound.name == name);
            if (s == null)
            {
                Debug.LogWarning("Sound " + name + " not Found!");
                return;
            }
            if (!s.source.isPlaying)
            {
                s.source.Play();
            }

        }
        else
        {
            Sound s = Array.Find(sounds, sound => sound.name == name);
            if (s == null)
            {
                Debug.LogWarning("Sound " + name + " not Found!");
                return;
            }
            if (!s.source.isPlaying)
            {
                s.source.Stop();
            }
        }
    }
    public void StopIfIsPlaing(string nameToCheck, string nameToStop)
    {
        Sound s = Array.Find(sounds, sound => sound.name == nameToCheck);
        if (s == null)
        {
            Debug.LogWarning("Sound " + name + " not Found!");
            return;
        }
        if (s.source.isPlaying)
        {
            Stop(nameToStop);
        }
    }


}
