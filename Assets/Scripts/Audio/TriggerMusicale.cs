using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMusicale : MonoBehaviour
{
    public string startSoundName;

    
    public string stopSoundName;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("TriggerMusicale: OnTriggerEnter2D called");
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("TriggerMusicale: Player collided with trigger. Starting sound: " + startSoundName);
            AudioManager.instance.Play(startSoundName);

            Debug.Log("TriggerMusicale: Stopping sound: " + stopSoundName);
            AudioManager.instance.Stop(stopSoundName);
        }
    }
}
