using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer2 : MonoBehaviour
{
    public string soundName;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {

            AudioManager.instance.Play(soundName);
        }
    }
}
