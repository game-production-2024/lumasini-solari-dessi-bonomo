using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;


public class Door : MonoBehaviour
{
    [Tooltip("Inserire il nome della scena nella quale entra il giocatore")]
    public string newScene;

    public UnityEvent MyEvent;

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            //Inserire messaggio che indica il tasto per aprire la porta
            if (Input.GetKeyDown(KeyCode.F))
            {
                AudioManager.instance.Play("Porta");
                other.GetComponent<PlayerInventory>().TakeKeyAway();
                SceneManager.LoadScene(newScene);
                MyEvent.Invoke();
            }
        }
    }
}