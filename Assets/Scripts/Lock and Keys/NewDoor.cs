using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class NewDoor : MonoBehaviour
{
    [Tooltip("Inserire il nome della scena nella quale entra il giocatore")]
    public string newScene;

    public UnityEvent MyEvent;

    private bool playerInTrigger = false;

    private PlayerInventory player;

    private void Update()
    {
        if (playerInTrigger && Input.GetKeyDown(KeyCode.F))
        {
            AudioManager.instance.Play("Porta");
            if (player != null)
            {
                player.GetComponent<PlayerInventory>().TakeKeyAway();
            }
            SceneManager.LoadScene(newScene);
            MyEvent.Invoke();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            playerInTrigger = true;
            player = other.transform.GetComponentInParent<PlayerInventory>();
            // Inserire qui il messaggio che indica il tasto per aprire la porta
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            playerInTrigger = false;
        }
    }
}
