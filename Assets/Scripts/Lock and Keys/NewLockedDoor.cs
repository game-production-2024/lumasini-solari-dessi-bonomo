using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class NewLockedDoor : MonoBehaviour
{
    [Tooltip("Inserire il nome della scena nella quale entra il giocatore")]
    public string newScene;

    public UnityEvent MyEvent;

    private bool playerInTrigger = false;
    private PlayerInventory playerInventory;

    private void Update()
    {
        if (playerInventory != null)
        {
            Debug.Log("Player hasKey: " + playerInventory.hasKey);
        }
        if (playerInTrigger && playerInventory != null && playerInventory.hasKey && Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log("Key pressed and player has key");
            MyEvent.Invoke();
            OpenDoor();
            playerInventory.hasKey = false;
            //playerInventory.UpdateKeyUI(); // Aggiorna la UI per nascondere la chiave
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Player entered trigger");
            playerInTrigger = true;
            playerInventory = other.GetComponent<PlayerInventory>();
            // Inserire qui il messaggio che indica il tasto per aprire la porta
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Player exited trigger");
            playerInTrigger = false;
            playerInventory = null;
        }
    }

    private void OpenDoor()
    {
        Debug.Log("Opening door, loading scene: " + newScene);
        // Implementa l'animazione di apertura della porta qui
        AudioManager.instance.Play("Porta");
        SceneManager.LoadScene(newScene);
    }
}
