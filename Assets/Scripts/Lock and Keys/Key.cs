using UnityEngine;
using UnityEngine.Events;

public class Key : MonoBehaviour
{
    public string keyName;

    public UnityEvent MyEvent;
    
    [HideInInspector]public Sprite keySprite;
    [HideInInspector]public bool isActive = true;

    private void Start()
    {
        keySprite = GetComponent<SpriteRenderer>().sprite;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerInventory playerInventory = other.GetComponent<PlayerInventory>();
            if (playerInventory != null)
            {
                AudioManager.instance.Play("Chiave");
                playerInventory.GiveKeyObject(this);
                gameObject.SetActive(false); // Rimuovi la chiave dalla scena
                MyEvent.Invoke();
            }
        }
    }
}