using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;


public class LockedDoor : MonoBehaviour
{
    [Tooltip("Inserire il nome della scena nella quale entra il giocatore")]
    public string newScene;

    public UnityEvent MyEvent;

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            //Inserire messaggio che indica il tasto per aprire la porta
            PlayerInventory playerInventory = other.GetComponent<PlayerInventory>();
            if (playerInventory != null && playerInventory.hasKey && Input.GetKey(KeyCode.F))
            {
                MyEvent.Invoke();
                OpenDoor();
                playerInventory.hasKey = false;
                //playerInventory.UpdateKeyUI(); // Aggiorna la UI per nascondere la chiave
            }
        }
    }

    private void OpenDoor()
    {
        // Implementa l'animazione di apertura della porta qui
        AudioManager.instance.Play("Porta");
        SceneManager.LoadScene(newScene);
    }
}
